# video Player for Sonja Nillson's "Rosas" Installation
# runs on raspberry-pi and listens for play, pause, stop
# messages via zmq

import zmq
import json
import socket
import glob
import os
import subprocess
import time
import RPi.GPIO as GPIO

DEBUG = False

def debug(arg):
    if DEBUG:
        print arg

r1 = 7
r2 = 11

if glob.glob("/videos/ANALOG*"):
    playcmd = ["omxplayer"]
else:
    playcmd = "omxplayer -o hdmi"
    playcmd = playcmd.split()

videopath = "/videos"


class Player():
    def __init__(self, plid=None, master=None, movie=None):
        #find my movie file and id if not set
        if movie is None:
            movie = glob.glob(videopath + "/PLAYER*")[0]
        if plid is None:
            plid = os.path.basename(movie)

        debug("player id: " + plid)
        debug("player for file: " + movie)

        #setup GPIOs for relays
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(r1, GPIO.OUT)
        GPIO.setup(r2, GPIO.OUT)
        GPIO.output(r1, True)
        GPIO.output(r2, True)

        #set play command
        self.playcmd = playcmd
        self.playcmd.append(movie)

        #if master is not specified, listen on bcast udp port 8881 for address
        if master is None:
            debug("looking for master on local network")
            bcast_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            bcast_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            bcast_socket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
            bcast_socket.bind(('', 8881))
            while master is None:
                message, address = bcast_socket.recvfrom(8192)
                if message.startswith("port"):
                    debug("found master " + message + " from " + address[0])
                    master = "tcp://" + address[0] + ":" + message.split()[1]

        self._context = zmq.Context()
        self.socket = self._context.socket(zmq.SUB)
        self.socket.setsockopt(zmq.SUBSCRIBE, plid)
        self.socket.connect(master)
        debug("connected to master " + master)

        self.plid = plid
        self.movie = movie
        self.cur_state = None
        self.proc = None
        self.procinit()

    def play(self):
        if self.procstate == "paused":
            #self.proc.communicate("p")
            self.proc.stdin.write("p")
            self.procstate = "playing"

    def black(self):
        if self.procstate == "playing":
            os.system("clear")
            self.proc.stdin.write("i")
            time.sleep(0.5)
            self.procstate = "paused"
            self.proc.stdin.write("p")

    def lightsoff(self):
        GPIO.output(r1, True)
        GPIO.output(r2, True)

    def lightson(self):
        GPIO.output(r1, False)
        GPIO.output(r2, False)

    def procinit(self):
        self.proc = subprocess.Popen(self.playcmd, stdin=subprocess.PIPE)
        self.procstate = "playing"
        time.sleep(2)
        self.black()

    def run(self):
        while True:
            # setup video if we don't have it yet
            if self.proc is None:
                self.procinit()

            # respawn the player process if it has terminated
            if self.proc.poll() is not None:
                self.procinit()

            # wait for message
            msg = self.socket.recv()
            msg = msg.lstrip(self.plid + " ")
            msg = json.loads(msg)
            # extract message
            state = msg["state"]

            if state != self.cur_state:
                self.cur_state = state
                debug("changing state to " + state)
            else:
                debug("ignoring change to same state")
                continue

            # if play play movie, turn on lights
            if state == "play":
                self.play()

            # if pause, pause movie
            elif state == "black":
                self.black()

            # lights on
            elif state == "lightson":
                self.lightson()

            elif state == "lightsoff":
                self.lightsoff()

            # else error
            else:
                debug("Error! Unrecognized playback state: " + state)

if __name__ == "__main__":
    player = Player()
    player.run()

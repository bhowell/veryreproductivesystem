Veryreproductivesystem is a collection of hacks that will let you use cheap embedded Linux computers like the Raspberry Pi to make synchronized audio-visual installations.  

Right now it's just a rather messy collection of scripts and config files, but the goal is to have something that one can download and run to turn any standard Debian/GNU machine into a node for a repro-system.  

Current setup requires all boxes be connected to the same ethernet wired ethernet switch but this could be modified to use WiFi.

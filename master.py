import json
import zmq
#import random
import time
import socket
import threading
import csv

DEBUG = False

def debug(arg):
    if DEBUG:
       print arg

#TODO: make example file.  last event
test_states = ["play", "stop", "borf", "pause"]

csvfile = "/videos/MASTER.csv"


class Master():
    def __init__(self, csvfile=None):
        self._zmqcontext = zmq.Context()
        self.socket = self._zmqcontext.socket(zmq.PUB)
        self.socket.bind("tcp://*:5556")
        self.puburi = "port 5556"

        #read csv file
        #if

        #ids come from first line

        #read in events

        #load event queue

    def run(self):
        #start announcer
        self.announcer = threading.Thread(target=self.announce)
        self.announcer.start()

        while True:
            with open(csvfile, "rU") as playlist:
                reader = csv.DictReader(playlist, delimiter=";")
                debug(reader.fieldnames)
                for row in reader:
                    debug(row)
                    dt = row["time"]
                    for player in reader.fieldnames:
                        if player.lower().startswith("player"):
                            debug(player + row[player])
                            msg = {}
                            msg["state"] = row[player]
                            self.socket.send(player + json.dumps(msg))
                        else:
                            debug("event len: " + row[player])
                            dt = row[player]
                    time.sleep(float(dt))
                    debug("waiting for next event")

            #msg = {}
            #msg["state"] = random.choice(test_states)

            #self.socket.send("PLAYER1.mov " + json.dumps(msg))
            #time.sleep(random.randint(1,5))

    def announce(self):
        #announce event publisher on port 8881 broadcast
        bcast_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        bcast_socket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        while True:
            bcast_socket.sendto(self.puburi, ('<broadcast>', 8881))
            time.sleep(1)


if __name__ == "__main__":
    master = Master()
    master.run()

#!/bin/bash
setterm -clear all -foreground black -hbcolor black -cursor off -store
cd /videos/bin
./player.sh &
if [ -f /videos/MASTER.csv ]
then
    echo Found MASTER.csv
    echo Starting master video server in 30 seconds
    sleep 30
    ./master.sh &
fi
